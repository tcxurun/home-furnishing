package com.ruyuan2020.furnishing.payment.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruyuan2020.common.domain.BaseDO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@TableName("ry_payment_transaction")
public class PaymentTransactionDO extends BaseDO {

    private String tradeNo;

    private Long memberId;

    private String memberUsername;

    private String paymentMethod;

    private BigDecimal amount;

    private String transactionNo;

    private Integer status;

    private LocalDateTime gmtFinished;
}
