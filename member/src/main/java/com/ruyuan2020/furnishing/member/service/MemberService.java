package com.ruyuan2020.furnishing.member.service;

import com.ruyuan2020.furnishing.member.domain.MemberDTO;

public interface MemberService {

    Long save(MemberDTO memberDTO);

    MemberDTO getMember(Long memberId);

    MemberDTO getByUsername(String username);
}
