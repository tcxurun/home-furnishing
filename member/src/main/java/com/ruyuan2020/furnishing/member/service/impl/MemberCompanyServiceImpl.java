package com.ruyuan2020.furnishing.member.service.impl;

import com.ruyuan2020.furnishing.member.dao.MemberCompanyDAO;
import com.ruyuan2020.furnishing.member.service.MemberCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MemberCompanyServiceImpl implements MemberCompanyService {

    @Autowired
    private MemberCompanyDAO memberCompanyDAO;

    @Override
    @Transactional
    public void informBidCompletedEvent(Long memberId) {
        memberCompanyDAO.updateBidQuantity(memberId);
    }

    @Override
    @Transactional
    public void informSignedEvent(Long memberId) {
        memberCompanyDAO.updateSignedQuantity(memberId);
    }

    @Override
    @Transactional
    public void informCommentEvent(Long memberId) {
        memberCompanyDAO.updateCommentQuantity(memberId);
    }
}
