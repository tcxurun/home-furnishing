package com.ruyuan2020.furnishing.member.constant;

import java.math.BigDecimal;

public class PointConstants {

    public static final BigDecimal POINT_RATE = BigDecimal.valueOf(5);
}
