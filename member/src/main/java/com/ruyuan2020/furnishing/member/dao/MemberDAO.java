package com.ruyuan2020.furnishing.member.dao;

import com.ruyuan2020.furnishing.common.dao.BaseDAO;
import com.ruyuan2020.furnishing.member.domain.MemberDO;

import java.util.Optional;

public interface MemberDAO extends BaseDAO<MemberDO> {

    Optional<MemberDO> getMemberById(Long id);

    Optional<MemberDO> getByUsername(String username);

    long countByUsername(String username);
}
