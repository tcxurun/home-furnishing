package com.ruyuan2020.furnishing.member.dao.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruyuan2020.furnishing.common.dao.impl.BaseDAOImpl;
import com.ruyuan2020.furnishing.member.dao.MemberDAO;
import com.ruyuan2020.furnishing.member.domain.MemberDO;
import com.ruyuan2020.furnishing.member.mapper.MemberMapper;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class MemberDAOImpl extends BaseDAOImpl<MemberMapper, MemberDO> implements MemberDAO {


    @Override
    public Optional<MemberDO> getMemberById(Long id) {
        LambdaQueryWrapper<MemberDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(MemberDO::getUsername);
        queryWrapper.eq(MemberDO::getId, id);
        return Optional.ofNullable(mapper.selectOne(queryWrapper));
    }

    @Override
    public Optional<MemberDO> getByUsername(String username) {
        LambdaQueryWrapper<MemberDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MemberDO::getUsername, username);
        return Optional.ofNullable(mapper.selectOne(queryWrapper));
    }

    @Override
    public long countByUsername(String username) {
        LambdaQueryWrapper<MemberDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(MemberDO::getUsername, username);
        return mapper.selectCount(queryWrapper);
    }
}
