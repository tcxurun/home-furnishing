package com.ruyuan2020.furnishing.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan2020.furnishing.member.domain.MemberCompanyDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface MemberCompanyMapper extends BaseMapper<MemberCompanyDO> {

    @Update("update ry_member_company set bid_quantity = bid_quantity+1 where member_id=#{memberId}")
    void updateBidQuantity(@Param("memberId") Long memberId);

    @Update("update ry_member_company set signed_quantity = signed_quantity+1 where member_id=#{memberId}")
    void updateSignedQuantity(@Param("memberId") Long memberId);

    @Update("update ry_member_company set comment_quantity = comment_quantity+1 where member_id=#{memberId}")
    void updateCommentQuantity(@Param("memberId") Long memberId);
}
