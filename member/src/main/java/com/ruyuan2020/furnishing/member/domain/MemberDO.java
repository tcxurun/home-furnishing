package com.ruyuan2020.furnishing.member.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruyuan2020.common.domain.BaseDO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@TableName("ry_member")
public class MemberDO extends BaseDO {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 会员类型
     */
    private String type;

    /**
     * 邮箱
     */
    private String mail;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 所在城市id
     */
    private Long cityId;

    /**
     * 真实姓名
     */
    private String realName;
}
