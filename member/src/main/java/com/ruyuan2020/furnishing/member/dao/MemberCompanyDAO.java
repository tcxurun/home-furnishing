package com.ruyuan2020.furnishing.member.dao;

import com.ruyuan2020.furnishing.common.dao.BaseDAO;
import com.ruyuan2020.furnishing.member.domain.MemberCompanyDO;

public interface MemberCompanyDAO extends BaseDAO<MemberCompanyDO> {

    void updateBidQuantity(Long memberId);

    void updateSignedQuantity(Long memberId);

    void updateCommentQuantity(Long memberId);
}
