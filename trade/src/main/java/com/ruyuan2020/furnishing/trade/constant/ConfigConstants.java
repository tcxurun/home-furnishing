package com.ruyuan2020.furnishing.trade.constant;

import java.math.BigDecimal;

public class ConfigConstants {

    public static final BigDecimal minPay = BigDecimal.valueOf(50);
}
