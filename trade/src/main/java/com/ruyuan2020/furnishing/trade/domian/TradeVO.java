package com.ruyuan2020.furnishing.trade.domian;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class TradeVO extends BaseDomain {

    private String paymentMethod;

    private BigDecimal amount;

    private BigDecimal gold;

    private Long memberId;

    private Long tenderId;
}
