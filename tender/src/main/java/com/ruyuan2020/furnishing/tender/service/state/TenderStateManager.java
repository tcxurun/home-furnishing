package com.ruyuan2020.furnishing.tender.service.state;

import com.ruyuan2020.furnishing.tender.domain.TenderDTO;

/**
 * 招标信息状态管理器接口
 */
public interface TenderStateManager {

    /**
     * 执行提交招标信息的操作
     *
     * @param tenderDTO 招标信息
     */
    void submit(TenderDTO tenderDTO);

    /**
     * 判断当前招标信息能否执行投标
     *
     * @param tenderDTO 招标信息
     * @return 能为执行投标
     */
    Boolean canBid(TenderDTO tenderDTO);

    /**
     * 执行投标的操作
     *
     * @param tenderDTO 招标信息
     */
    void bid(TenderDTO tenderDTO);

    /**
     * 判断当前招标信息能否执行签约
     *
     * @param tenderDTO 招标信息
     * @return 能为执行签约
     */
    Boolean canSign(TenderDTO tenderDTO);

    /**
     * 执行签约的操作
     *
     * @param tenderDTO 招标信息
     */
    void sign(TenderDTO tenderDTO);

    /**
     * 判断当前招标信息能否执行完成
     *
     * @param tenderDTO 招标信息
     * @return 能为执行完成
     */
    Boolean canComplete(TenderDTO tenderDTO);

    /**
     * 执行完成的操作
     *
     * @param tenderDTO 招标信息
     */
    void complete(TenderDTO tenderDTO);

    /**
     * 判断当前招标信息能否执行评论
     *
     * @param tenderDTO 招标信息
     * @return 能为执行评论
     */
    Boolean canComment(TenderDTO tenderDTO);
}
