package com.ruyuan2020.furnishing.tender.dao;

import com.ruyuan2020.furnishing.common.dao.BaseDAO;
import com.ruyuan2020.furnishing.tender.domain.TenderDO;

import java.util.Optional;

public interface TenderDAO extends BaseDAO<TenderDO> {

    String getStatus(Long id);

    void updatePay(Long id);

    Optional<TenderDO> getTrustInfoById(Long id);

    Optional<TenderDO> getBiddingInfoById(Long id);

    Optional<TenderDO> getSigningInfoById(Long id);

    void updateStatus(Long id, String status);

    Optional<TenderDO> getCompletionInfoById(Long id);
}
