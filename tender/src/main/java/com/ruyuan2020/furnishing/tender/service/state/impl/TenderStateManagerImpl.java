package com.ruyuan2020.furnishing.tender.service.state.impl;

import com.ruyuan2020.furnishing.tender.domain.TenderDTO;
import com.ruyuan2020.furnishing.tender.service.state.TenderState;
import com.ruyuan2020.furnishing.tender.service.state.TenderStateFactory;
import com.ruyuan2020.furnishing.tender.service.state.TenderStateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TenderStateManagerImpl implements TenderStateManager {

    @Autowired
    private WaitingTenderState waitingTenderState;

    @Autowired
    private SignedTenderState signedTenderState;

    @Autowired
    private CompletionTenderState completionTenderState;

    @Autowired
    private TenderStateFactory tenderStateFactory;

    @Override
    public void submit(TenderDTO tenderDTO) {
        waitingTenderState.doTransition(tenderDTO);
    }

    @Override
    public Boolean canBid(TenderDTO tenderDTO) {
        TenderState state = tenderStateFactory.get(tenderDTO);
        return state.canBid(tenderDTO);
    }

    @Override
    public void bid(TenderDTO tenderDTO) {
        waitingTenderState.doTransition(tenderDTO);
    }

    @Override
    public Boolean canSign(TenderDTO tenderDTO) {
        TenderState state = tenderStateFactory.get(tenderDTO);
        return state.canSign(tenderDTO);
    }

    @Override
    public void sign(TenderDTO tenderDTO) {
        signedTenderState.doTransition(tenderDTO);
    }

    @Override
    public Boolean canComplete(TenderDTO tenderDTO) {
        TenderState state = tenderStateFactory.get(tenderDTO);
        return state.canComplete(tenderDTO);
    }

    @Override
    public void complete(TenderDTO tenderDTO) {
        completionTenderState.doTransition(tenderDTO);
    }

    @Override
    public Boolean canComment(TenderDTO tenderDTO) {
        TenderState state = tenderStateFactory.get(tenderDTO);
        return state.canComment(tenderDTO);
    }
}
