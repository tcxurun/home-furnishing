package com.ruyuan2020.furnishing.tender.service.state.impl;

import com.ruyuan2020.furnishing.tender.constant.TenderStatus;
import com.ruyuan2020.furnishing.tender.dao.TenderDAO;
import com.ruyuan2020.furnishing.tender.domain.TenderDO;
import com.ruyuan2020.furnishing.tender.domain.TenderDTO;
import com.ruyuan2020.furnishing.tender.service.state.TenderState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 结束招标状态
 */
@Component
public class CompletionTenderState implements TenderState {

    @Autowired
    private TenderDAO tenderDAO;

    @Override
    public void doTransition(TenderDTO tenderDTO) {
        tenderDTO.setStatus(TenderStatus.COMPLETED_TENDER);
        TenderDO tenderDO = tenderDTO.clone(TenderDO.class);
        tenderDAO.update(tenderDO);
    }

    @Override
    public Boolean canBid(TenderDTO tenderDTO) {
        return false;
    }

    @Override
    public Boolean canSign(TenderDTO tenderDTO) {
        return false;
    }

    @Override
    public Boolean canComplete(TenderDTO tenderDTO) {
        return false;
    }

    @Override
    public Boolean canComment(TenderDTO tenderDTO) {
        return true;
    }
}
