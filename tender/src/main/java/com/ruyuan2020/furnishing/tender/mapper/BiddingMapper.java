package com.ruyuan2020.furnishing.tender.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan2020.furnishing.tender.domain.BiddingDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BiddingMapper extends BaseMapper<BiddingDO> {

}
