package com.ruyuan2020.furnishing.tender.constant;

/**
 * 招标信息状态
 */
public class TenderStatus {

    /**
     * 招标中
     */
    public static final String WAITING_TENDER = "00";

    /**
     * 投标结束
     */
    public static final String END_TENDER = "01";

    /**
     * 已签约
     */
    public static final String SIGNED_TENDER = "02";

    /**
     * 已完成工程
     */
    public static final String COMPLETED_TENDER = "03";
}
