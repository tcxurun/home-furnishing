package com.ruyuan2020.furnishing.tender.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompletionVO extends BaseDomain {

    /**
     * 招标id
     */
    private Long tenderId;

    private Long memberId;
}
