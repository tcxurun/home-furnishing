package com.ruyuan2020.furnishing.tender.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SigningDTO extends BaseDomain {

    private Long biddingId;

    private Long memberId;
}
