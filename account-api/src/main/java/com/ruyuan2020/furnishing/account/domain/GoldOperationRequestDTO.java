package com.ruyuan2020.furnishing.account.domain;

import com.ruyuan2020.common.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class GoldOperationRequestDTO extends BaseDomain {

    private Long memberId;

    private BigDecimal number;

    private String log;
}
