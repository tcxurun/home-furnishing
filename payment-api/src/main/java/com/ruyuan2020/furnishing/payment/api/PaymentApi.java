package com.ruyuan2020.furnishing.payment.api;

import com.ruyuan2020.common.exception.BusinessException;
import com.ruyuan2020.furnishing.payment.domain.PaymentDTO;

import java.util.concurrent.CompletableFuture;

public interface PaymentApi {

    /**
     * 构建支付URL
     *
     * @param paymentDTO 支付信息
     * @return 支付URL
     */
    String buildPayUrl(PaymentDTO paymentDTO) throws BusinessException;
}
